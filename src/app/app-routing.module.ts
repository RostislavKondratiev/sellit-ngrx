import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import {IsLoginGuard} from './guards/isLogin.guard';

const GUARDS = [
  AuthGuard,
  IsLoginGuard,
];

const ROUTES: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'products'},
  {path: 'authorization', loadChildren: './authorization/authorization.module#AuthorizationModule', canActivate: [AuthGuard]},
  {path: 'products', loadChildren: './products/products.module#ProductsModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  providers: [...GUARDS],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
