import { Component } from '@angular/core';

@Component({
  selector: 'si-root',
  styleUrls: ['/app.component.scss'],
  template: `
    <si-header></si-header>
    <router-outlet></router-outlet>
    <si-footer></si-footer>
  `,
})
export class AppComponent {
}
