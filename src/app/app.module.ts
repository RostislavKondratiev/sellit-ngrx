import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { InitializeService } from './services/initialize.service';
import { CustomSerializer, reducers, effects } from './store';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GlobalModule } from './shared/global/global.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

const environment = {
  development: true,
  production: false,
};

export function checkUser(init: InitializeService) {
  return () => init.checkUser();
}

const PROVIDERS = [
  {provide: APP_INITIALIZER, useFactory: checkUser, deps: [InitializeService], multi: true},
  {provide: RouterStateSerializer, useClass: CustomSerializer},
  InitializeService
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule.forRoot(),
    GlobalModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'routerReducer'
    }),
    environment.development ? StoreDevtoolsModule.instrument() : [],
    AppRoutingModule
  ],
  providers: [
    ...PROVIDERS
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
