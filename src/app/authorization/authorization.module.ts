import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './containers/login/login.component';
import { SignupComponent } from './containers/signup/signup.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { AuthorizationRoutingModule } from './authorization-routing.module';
import { SiFormsModule } from '../shared/forms/si-forms.module';

const COMPONENTS = [
  LoginComponent,
  SignupComponent,
  LoginFormComponent,
  SignupFormComponent
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SiFormsModule,
    AuthorizationRoutingModule
  ],
  declarations: [
    ...COMPONENTS
  ],
  providers: [],
  exports: []
})
export class AuthorizationModule {
}
