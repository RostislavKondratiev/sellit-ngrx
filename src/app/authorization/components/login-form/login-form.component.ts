import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'si-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  @Output() public submitForm = new EventEmitter<any>();
  public loginForm = new FormGroup(
    {
      email: new FormControl(null),
      password: new FormControl(null)
    }
  );

  public login($event) {
    $event.preventDefault();
    this.submitForm.emit(this.loginForm.value);
  }
}
