import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';


@Component({
  selector: 'si-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent {
  @Output() public submit = new EventEmitter<any>();
  public signUpForm = new FormGroup({
    email: new FormControl(null, [Validators.required]),
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required]),
    password_confirm: new FormControl(null, [Validators.required])
  });

  public signUp($event) {
    $event.preventDefault();
    this.submit.emit(this.signUpForm.value);
  }
}
