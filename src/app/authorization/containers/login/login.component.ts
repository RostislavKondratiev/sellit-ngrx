import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromStore from '../../../store';

@Component({
  selector: 'si-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private store: Store<any>) {
  }

  public submit(data) {
    this.store.dispatch(new fromStore.Login(data));
  }
}
