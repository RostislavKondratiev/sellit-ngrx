import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromStore from '../../../store';

@Component({
  selector: 'si-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  constructor(private store: Store<any>) {
  }

  public submit(data) {
    this.store.dispatch(new fromStore.SignUp(data));
  }
}
