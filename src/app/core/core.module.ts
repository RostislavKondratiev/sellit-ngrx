import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


import { AuthService } from './services/auth.service';
import { LocalStorageService } from './services/localstorage.service';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ProfileService } from './services/profile.service';


const MODULES = [
  CommonModule,
  HttpClientModule,
];

const COMPONENTS = [
];

const INTERCEPTORS = [
  {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
];

const PROVIDERS = [
  AuthService,
  LocalStorageService,
  ProfileService,
];

@NgModule({
  imports: [
    ...MODULES
  ],
  declarations: [
    ...COMPONENTS
  ],
  providers: [
    ...INTERCEPTORS
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class CoreModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        ...PROVIDERS,
        ...INTERCEPTORS
      ]
    };
  }
}
