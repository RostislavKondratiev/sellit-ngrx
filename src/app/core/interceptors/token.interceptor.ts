import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';

import * as rootStore from '../../store';
import { switchMap, take } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private store: Store<rootStore.RootState>) {
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler) {
    return this.store.select(rootStore.getToken)
      .pipe(
        take(1),
        switchMap((token) => {
          console.log(request);
          if (token) {
            request = request.clone({
              setHeaders: {
                Authorization: `Token ${token}`
              }
            });
          }
          console.log(request);
          return next.handle(request);
        })
      );
  }
}
