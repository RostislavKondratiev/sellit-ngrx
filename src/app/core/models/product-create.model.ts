export interface ProductCreateModel {
  title: string;
  description: string;
  author: number;
  photos: number[];
  price: number;
  date_create?: Date;
}
