export interface Photo {
  photo?: any;
}

export interface AuthorDetails {
  id: number;
  first_name: string;
  last_name: string;
  username: string;
  email: string;
  photo: Photo;
}

export interface PhotoDetail {
  id: number;
  photo: string;
}

export interface ProductDetails {
  id: number;
  title: string;
  description: string;
  author: number;
  author_details: AuthorDetails;
  price: string;
  photos: number[];
  photo_details: PhotoDetail[];
  date_create: Date;
  date_update: Date;
}
