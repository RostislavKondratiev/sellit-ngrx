import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../../helpers/api.config';
import { LoginModel, SignUpModel } from '../models/auth.model';
import { catchError } from 'rxjs/operators';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {
  }

  public signUp(data: SignUpModel) {
    return this.http.post(`${ApiConfig.base}/signup/`, data)
      .pipe(
        catchError((error) => _throw(error.json))
      );
  }

  public login(data: LoginModel) {
    return this.http.post(`${ApiConfig.base}/login/`, data)
      .pipe(
        catchError((error) => _throw(error.json()))
      );
  }
}
