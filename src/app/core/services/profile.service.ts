import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../../helpers/api.config';
import { catchError } from 'rxjs/operators';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class ProfileService {
  constructor(private http: HttpClient) {
  }

  public getUserData() {
    return this.http.get(ApiConfig.user)
      .pipe(
        catchError((error) => _throw(error.json()))
      );
  }
}
