import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import * as rootStore from '../store';
import {map, take} from 'rxjs/operators';

@Injectable()
export class IsLoginGuard implements CanActivate {
  constructor(private store: Store<rootStore.RootState>, private router: Router) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select(rootStore.getUser)
      .pipe(
        take(1),
        map((user) => {
          if (user) {
            return true;
          } else {
            this.router.navigate(['/products']);
          }
        })
      );
  }
}
