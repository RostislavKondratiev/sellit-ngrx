export class ApiConfig {
  // public static base = 'http://fe-kurs.light-it.net:38000/api';
  public static base = 'http://fe-kurs.light-it.loc/api';
  public static products = `${ApiConfig.base}/poster`;
  public static user = `${ApiConfig.base}/profile/me`;
  public static photo = `${ApiConfig.base}/photo/`;
  public static productCreate = `${ApiConfig.base}/poster/`;
}
