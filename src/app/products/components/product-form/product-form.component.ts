import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as rootStore from './../../../store';
import * as productStore from './../../store';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'si-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductFormComponent implements OnInit, OnChanges, OnDestroy {
  public productForm: FormGroup = null;
  @Input() public files = [];
  @Output() public submitForm = new EventEmitter();
  @Output() public remove = new EventEmitter();
  @Output() public filesChanges = new EventEmitter();
  private until = new Subject();

  constructor(private store: Store<rootStore.RootState | productStore.ProductsState>,
              private cd: ChangeDetectorRef) {
  }

  public ngOnChanges(changes) {
    console.log(changes);
  }

  public ngOnInit() {
    this.store.select(rootStore.getUser)
      .pipe(
        take(1)
      )
      .subscribe((user) => {
        this.productForm = new FormGroup(
          {
            title: new FormControl(null, [Validators.required]),
            price: new FormControl(null, [Validators.required]),
            author: new FormControl(user.id),
            description: new FormControl(null),
            photos: new FormControl(null),
            date_create: new FormControl(null)
          }
        );
      });
  }

  public ngOnDestroy() {
    this.until.next(true);
  }

  public removeImage(index) {
    this.remove.emit(index);
  }

  public fileInput($event) {
    const files = $event.target.files;
    if (files && files.length > 0) {
      for (const file of files) {
        const reader = new FileReader();
        reader.onloadend = () => {
          this.filesChanges.emit({file, photo: reader.result});
        };
        reader.readAsDataURL(file);
      }
    }
  }

  public submitProduct($event) {
    $event.preventDefault();
    this.submitForm.emit({files: this.files, form: this.productForm.value});
  }
}
