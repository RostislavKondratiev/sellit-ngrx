import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {ProductListItem} from '../../../core/models/product-list.model';

@Component({
  selector: 'si-products-item',
  templateUrl: './products-item.component.html',
  styleUrls: ['./products-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsItemComponent {
  @Input() public product: ProductListItem;
  public photoNotFound = '/assets/img/image-not-found.jpg';
}
