import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {debounceTime, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'si-products-search',
  templateUrl: './products-search.component.html',
  styleUrls: ['./products-search.component.scss']
})
export class ProductsSearchComponent implements OnInit, OnDestroy {
  @Output() public search = new EventEmitter<string>();

  @Input('searchValue')
  public set searchValue(value: string) {
    this.searchControl.setValue(value);
  }

  public searchControl = new FormControl();
  public until = new Subject();


  public ngOnInit() {
    this.searchControl.valueChanges
      .pipe(
        debounceTime(300),
        takeUntil(this.until)
      )
      .subscribe((val) => {
        console.log(val);
        this.search.emit(val);
      });
  }

  public ngOnDestroy() {
    this.until.next(true);
  }
}
