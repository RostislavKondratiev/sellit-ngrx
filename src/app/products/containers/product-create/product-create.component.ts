import { ChangeDetectorRef, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as productStore from './../../store';
import { ProductsService } from '../../services/products.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'si-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent {
  public files = [];

  constructor(private store: Store<productStore.ProductsState>,
              private cd: ChangeDetectorRef) {
  }

  public submit(data) {
    const form = data.form;
    const files = data.files.map((item) => item.file);
    this.store.dispatch(new productStore.CreateProduct({files, form}));
  }

  public fileHandler(data) {
    this.files = [...this.files, data];
  }

  public remove(index) {
    this.files.splice(index, 1);
    this.files = [...this.files];
  }
}
