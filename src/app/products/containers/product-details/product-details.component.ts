import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import * as fromStore from './../../store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ProductDetails } from '../../../core/models/product-details.model';

@Component({
  selector: 'si-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  public product$: Observable<ProductDetails>;

  constructor(private store: Store<fromStore.ProductsState>) {
  }

  public ngOnInit() {
    this.product$ = this.store.select(fromStore.getDetails);
  }

  public ngOnDestroy() {
    this.store.dispatch(new fromStore.ClearProductDetails());
  }

}
