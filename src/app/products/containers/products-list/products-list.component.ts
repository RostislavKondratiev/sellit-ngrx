import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from './../../store';
import * as coreStore from '../../../store';
import {Observable} from 'rxjs/Observable';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'si-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsListComponent implements OnInit, OnDestroy {

  public products$: Observable<any>;
  public preloader$: Observable<boolean>;
  public searchValue: string;

  private until = new Subject();
  private meta;

  constructor(private store: Store<fromStore.ProductsState>) {
  }

  public ngOnInit() {
    this.products$ = this.store.select(fromStore.getAllProducts);
    this.preloader$ = this.store.select(fromStore.getProductsLoading);
    this.store.select(coreStore.selectRouterState)
      .pipe(
        takeUntil(this.until)
      )
      .subscribe((routerState) => {
        this.searchValue = routerState.state.queryParams.search;
      });
    this.store.select(fromStore.getProductsPagination)
      .pipe(
        takeUntil(this.until)
      )
      .subscribe((res) => {
        this.meta = res;
      });
  }

  public loadMore() {
    if (this.meta.next) {
      const data = {offset: this.meta.offset, limit: this.meta.limit, search: this.searchValue};
      this.store.dispatch(
        new fromStore.LoadProducts(data)
      );
    }

  }

  public search(search) {
    this.searchValue = search;
    const data = {offset: 0, limit: this.meta.limit, search: this.searchValue};
    this.store.dispatch(
      new fromStore.LoadSearchableProducts(data)
    );
  }

  public ngOnDestroy() {
    this.until.next(true);
  }
}
