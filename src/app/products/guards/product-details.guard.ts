import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';

import * as fromStore from './../store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map, skipWhile, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class ProductDetailsGuard implements CanActivate {
  constructor(private store: Store<fromStore.ProductsState>) {
  }

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return of(true)
      .pipe(
        map(() => this.store.dispatch(new fromStore.LoadProductDetails(+route.params.id))),
        switchMap(() => {
          return this.store.select(fromStore.getDetailsLoaded).pipe(
            skipWhile((val) => !val)
          );
        })
      );
  }
}
