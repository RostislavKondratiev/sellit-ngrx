import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromStore from './../store';
import * as coreStore from '../../store';
import {of} from 'rxjs/observable/of';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProductsListGuard implements CanActivate {

  constructor(private store: Store<any>) {
  }

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    // return of(true).pipe(
    //   tap(() => this.store.dispatch(new fromStore.LoadProducts({offset: 0, limit: 12, search: route.queryParams.search}))),
    // );
    return this.store.select(coreStore.selectRouterState)
      .pipe(
        tap((routerState) => {
          const data = {offset: 0, limit: 12, search: routerState.state.queryParams.search};
          return this.store.dispatch(new fromStore.LoadProducts(data));
        }),
        map(() => true)
      );
  }
}
