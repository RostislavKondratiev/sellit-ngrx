import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsListComponent } from './containers/products-list/products-list.component';
import { ProductsListGuard } from './guards/products-list.guard';
import { ProductDetailsComponent } from './containers/product-details/product-details.component';
import { ProductDetailsGuard } from './guards/product-details.guard';
import {ProductCreateComponent} from './containers/product-create/product-create.component';
import {IsLoginGuard} from '../guards/isLogin.guard';

const GUARDS = [
  ProductsListGuard,
  ProductDetailsGuard,
];

const ROUTES: Routes = [
  {path: '', component: ProductsListComponent, canActivate: [ProductsListGuard]},
  {path: 'create', component: ProductCreateComponent, canActivate: [IsLoginGuard]},
  {path: ':id', component: ProductDetailsComponent, canActivate: [ProductDetailsGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  providers: [...GUARDS],
  exports: [RouterModule]
})
export class ProductsRoutingModule {

}
