import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsListComponent } from './containers/products-list/products-list.component';
import { ProductsItemComponent } from './components/products-item/products-item.component';
import { ProductsService } from './services/products.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { effects } from './store/effects';
import { PreloaderModule } from '../shared/preloader/preloader.module';
import { ProductDetailsComponent } from './containers/product-details/product-details.component';
import { DirectivesModule } from '../shared/directives/directives.module';
import { ProductsSearchComponent } from './components/products-search/products-search.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { ProductCreateComponent } from './containers/product-create/product-create.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { SiFormsModule } from '../shared/forms/si-forms.module';
import {CarouselModule} from '../shared/carousel/carousel.module';

const COMPONENTS = [
  ProductsListComponent,
  ProductsItemComponent,
  ProductsSearchComponent,
  ProductDetailsComponent,
  ProductCreateComponent,
  ProductFormComponent
];

const PROVIDERS = [
  ProductsService,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ProductsRoutingModule,
    SiFormsModule,
    CarouselModule,
    CoreModule,
    PreloaderModule,
    DirectivesModule,
    StoreModule.forFeature('products', reducers),
    EffectsModule.forFeature(effects)
  ],
  declarations: [
    ...COMPONENTS
  ],
  providers: [
    ...PROVIDERS,
  ],
  exports: []
})
export class ProductsModule {

}
