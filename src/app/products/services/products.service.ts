import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiConfig } from '../../helpers/api.config';
import { ProductListResponse } from '../../core/models/product-list.model';
import { catchError } from 'rxjs/operators';
import { _throw } from 'rxjs/observable/throw';
import { ProductDetails } from '../../core/models/product-details.model';

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient) {
  }

  public getProductsList(offset = 0, limit = 12, search = null) {
    let params = new HttpParams();
    params = params.append('offset', offset.toString());
    params = params.append('limit', limit.toString());
    if (search) {
      params = params.append('search', search);
    }
    return this.http.get<ProductListResponse>(ApiConfig.products, {params})
      .pipe(
        catchError((error) => _throw(error))
      );
  }

  public getProductDetails(id: number) {
    return this.http.get<ProductDetails>(`${ApiConfig.products}/${id}`)
      .pipe(
        catchError((error) => _throw(error))
      );
  }

  public addPhotos(files) {
    console.log(files);
    const formData = new FormData();
    for (const file of files) {
      formData.append(file.name, file);
    }
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'multipart/form-data');
    console.log(headers.get('Content-Type'));
    return this.http.post(ApiConfig.photo,  formData, {headers})
      .pipe(
        catchError((error) => _throw(error))
      );
  }

  public createProduct(data) {
    return this.http.post(ApiConfig.productCreate, data)
      .pipe(
        catchError((error) => _throw(error))
      );
  }
}
