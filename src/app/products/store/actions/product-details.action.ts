import { Action } from '@ngrx/store';
import { ProductDetails } from '../../../core/models/product-details.model';

export const LOAD_PRODUCT_DETAILS = '[Product Details] Load Products Details';
export const LOAD_PRODUCT_DETAILS_SUCCESS = '[Product Details] Load Products Details Success';
export const LOAD_PRODUCT_DETAILS_FAIL = '[Product Details] Load Products Details Fail';
export const CLEAR_PRODUCT_DETAILS = '[Products Detail] Clear Product Details';


export class LoadProductDetails implements Action {
  readonly type = LOAD_PRODUCT_DETAILS;

  constructor(public payload: number) {
  }
}

export class LoadProductDetailsSuccess implements Action {
  readonly type = LOAD_PRODUCT_DETAILS_SUCCESS;

  constructor(public payload: ProductDetails) {
  }
}

export class LoadProductDetailsFail implements Action {
  readonly type = LOAD_PRODUCT_DETAILS_FAIL;

  constructor(public payload: any) {
  }
}

export class ClearProductDetails implements Action {
  readonly type = CLEAR_PRODUCT_DETAILS;
}


export type ProductDetailsAction =
  | LoadProductDetails
  | LoadProductDetailsSuccess
  | LoadProductDetailsFail
  | ClearProductDetails;

