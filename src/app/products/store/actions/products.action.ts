import {Action} from '@ngrx/store';
import {ProductListItem, ProductListResponse} from '../../../core/models/product-list.model';
import { ProductCreateModel } from '../../../core/models/product-create.model';

// load products
export const LOAD_PRODUCTS = '[Products] Load Products';
export const LOAD_SEARCHABLE_PRODUCTS = '[Products] Load Searchable Products';
export const LOAD_SEARCHABLE_PRODUCTS_SUCCESS = '[Products] Load Searchable Products Success';
export const LOAD_PRODUCTS_SUCCESS = '[Products] Load Products Success';
export const LOAD_PRODUCTS_FAIL = '[Products] Load Products Fail';

export const CREATE_PRODUCT = '[Products] Create Product';
export const CREATE_PRODUCT_SUCCESS = '[Products] Create Product Success';
export const CREATE_PRODUCT_FAIL = '[Products] Create Product Fail';

export class LoadProducts implements Action {
  readonly type = LOAD_PRODUCTS;

  constructor(public payload: { offset: number, limit?: number, search?: string }) {
  }
}

export class LoadSearchableProducts implements Action {
  readonly type = LOAD_SEARCHABLE_PRODUCTS;

  constructor(public payload: { offset: number, limit?: number, search?: string }) {
  }
}

export class LoadSearchableProductsSuccess implements Action {
  readonly type = LOAD_SEARCHABLE_PRODUCTS_SUCCESS;

  constructor(public payload: { data: ProductListResponse, search: string }) {
  }
}

export class LoadProductsSuccess implements Action {
  readonly type = LOAD_PRODUCTS_SUCCESS;

  constructor(public payload: ProductListResponse) {
  }
}

export class LoadProductsFail implements Action {
  readonly type = LOAD_PRODUCTS_FAIL;

  constructor(public payload: any) {
  }
}

export class CreateProduct implements Action {
  readonly type = CREATE_PRODUCT;

  constructor(public payload: {files: any, form: ProductCreateModel}) {
  }
}

export class CreateProductSuccess implements Action {
  readonly type = CREATE_PRODUCT_SUCCESS;

  constructor(public payload: any) {
  }
}

export class CreateProductFail implements Action {
  readonly type = CREATE_PRODUCT_FAIL;

  constructor(public payload: any) {
  }
}

export type ProductsAction = LoadProducts
  | LoadSearchableProducts
  | LoadSearchableProductsSuccess
  | LoadProductsSuccess
  | LoadProductsFail
  | CreateProduct
  | CreateProductSuccess
  | CreateProductFail;
