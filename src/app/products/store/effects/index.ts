import { ProductsEffect } from './products.effects';

export const effects: any[] = [ProductsEffect];

export * from './products.effects';
