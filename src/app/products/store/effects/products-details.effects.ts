// import { Injectable } from '@angular/core';
// import { ProductsService } from '../../services/products.service';
// import { Actions, Effect, ofType } from '@ngrx/effects';
//
// import * as productsActions from './../actions';
// import { catchError, map, switchMap } from 'rxjs/operators';
// import {of} from 'rxjs/observable/of';
// import { ProductDetails } from '../../../core/models/product-details.model';
//
// @Injectable()
// export class ProductsDetailsEffects {
//   constructor(private products: ProductsService,
//               private actions$: Actions) {
//   }
//
//   @Effect() public loadDetails$ = this.actions$
//     .pipe(
//       ofType(productsActions.LOAD_PRODUCT_DETAILS),
//       map((action: productsActions.ProductDetailsAction) => action.payload),
//       switchMap((id) => {
//         return this.products
//           .getProductDetails(id)
//           .pipe(
//             map((res: ProductDetails) => new productsActions.LoadProductDetailsSuccess(res)),
//             catchError((error) => of(new productsActions.LoadProductDetailsFail(error)))
//           );
//       })
//     );
// }
