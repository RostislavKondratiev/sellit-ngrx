import { Injectable } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromRoot from '../../../store';
import * as productsActions from './../actions';
import { of } from 'rxjs/observable/of';
import { ProductListResponse } from '../../../core/models/product-list.model';
import { ProductDetails } from '../../../core/models/product-details.model';
import { combineLatest } from 'rxjs/observable/combineLatest';

@Injectable()
export class ProductsEffect {
  constructor(private products: ProductsService,
              private actions$: Actions) {
  }

  @Effect() public loadProducts$ = this.actions$
    .pipe(
      ofType(productsActions.LOAD_PRODUCTS),
      map((action: productsActions.LoadProducts) => action.payload),
      switchMap((meta) => {
        console.log(meta);
        return this.products
          .getProductsList(meta.offset, meta.limit, meta.search)
          .pipe(
            map((res: ProductListResponse) => {
              return new productsActions.LoadProductsSuccess(res);
            }),
            catchError((error) => {
              console.log(error);
              return of(new productsActions.LoadProductsFail(error));
            })
          );
      })
    );

  @Effect() public loadSearchableProducts$ = this.actions$
    .pipe(
      ofType(productsActions.LOAD_SEARCHABLE_PRODUCTS),
      map((action: productsActions.LoadSearchableProducts) => action.payload),
      switchMap((meta) => {
        return this.products
          .getProductsList(meta.offset, meta.limit, meta.search)
          .pipe(
            map((res: ProductListResponse) => {
              return new productsActions.LoadSearchableProductsSuccess({data: res, search: meta.search});
            })
          );
      })
    );

  @Effect() public searchSuccess$ = this.actions$
    .pipe(
      ofType(productsActions.LOAD_SEARCHABLE_PRODUCTS_SUCCESS),
      map((action: productsActions.LoadSearchableProductsSuccess) => action.payload),
      map((payload) => payload.search === '' ? null : payload.search),
      map((search) => new fromRoot.Go({path: ['/products'], query: {search: search}}))
    );

  @Effect() public loadDetails$ = this.actions$
    .pipe(
      ofType(productsActions.LOAD_PRODUCT_DETAILS),
      map((action: productsActions.LoadProductDetails) => action.payload),
      switchMap((id) => {
        return this.products
          .getProductDetails(id)
          .pipe(
            map((res: ProductDetails) => new productsActions.LoadProductDetailsSuccess(res)),
            catchError((error) => of(new productsActions.LoadProductDetailsFail(error)).pipe(
              map(() => new fromRoot.Go({path: ['/products']}))
            ))
          );
      })
    );

  @Effect() public productCreate$ = this.actions$
    .pipe(
      ofType(productsActions.CREATE_PRODUCT),
      map((action: productsActions.CreateProduct) => action.payload),
      switchMap((data) => combineLatest(this.products.addPhotos(data.files), of(data.form))),
      switchMap(([ids, form]) => {
        return this.products
          .createProduct({...form, ...{photos: ids}})
          .pipe(
            map((res) => new productsActions.CreateProductSuccess(res))
          );
      }),
    );
}
