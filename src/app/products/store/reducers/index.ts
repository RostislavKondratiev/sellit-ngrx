import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as productsReducer from './products.reducer';
import * as detailsReducer from './product-details.reducer';

export interface ProductsState {
  list: productsReducer.ProductsListState;
  details: detailsReducer.ProductDetailsState;
}

export const reducers: ActionReducerMap<ProductsState> = {
  list: productsReducer.reducer,
  details: detailsReducer.reducer
};

export const getProductsState = createFeatureSelector<ProductsState>(
  'products'
);
