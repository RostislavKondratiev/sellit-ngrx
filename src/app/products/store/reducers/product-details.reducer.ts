import { ProductDetails } from '../../../core/models/product-details.model';
import * as productsActions from './../actions';


export interface ProductDetailsState {
  data: ProductDetails | any;
  loading: boolean;
  loaded: boolean;
}

export const InitialState = {
  data: {},
  loading: false,
  loaded: false
};

export function reducer(state: ProductDetailsState = InitialState, action: productsActions.ProductDetailsAction): ProductDetailsState {
  switch (action.type) {
    case productsActions.LOAD_PRODUCT_DETAILS:
      return {...state, loading: true};

    case productsActions.LOAD_PRODUCT_DETAILS_SUCCESS:
      const data = action.payload;
      return {
        ...state,
        data,
        loading: false,
        loaded: true
      };

    case productsActions.LOAD_PRODUCT_DETAILS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false
      };

    case productsActions.CLEAR_PRODUCT_DETAILS:
      return InitialState;
  }
  return state;
}

export const getProductsDetails = (state: ProductDetailsState) => state.data;
export const getProductsDetailsLoaded = (state: ProductDetailsState) => state.loaded;
export const getProductsDetailsLoading = (state: ProductDetailsState) => state.loading;
