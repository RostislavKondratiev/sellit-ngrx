import {ProductListItem} from '../../../core/models/product-list.model';
import * as productsActions from './../actions';

//tslint:disable

export interface ProductsListState {
  count: number;
  next: string;
  previous?: any;
  entities: { [id: number]: ProductListItem };
  loading: boolean;
  loaded: boolean;
  offset: number;
  errors: any;
}

export const InitialState = {
  entities: {},
  count: null,
  next: null,
  previous: null,
  loading: false,
  loaded: false,
  offset: 0,
  errors: null
};

export function reducer(state: ProductsListState = InitialState, action: productsActions.ProductsAction): ProductsListState {
  switch (action.type) {
    case productsActions.LOAD_PRODUCTS:
    case productsActions.LOAD_SEARCHABLE_PRODUCTS:
      return {...state, loading: true, errors: null};

    case productsActions.LOAD_PRODUCTS_SUCCESS: {
      const products = action.payload.results;
      let {count, next, previous} = action.payload;
      let offset = state.offset;
      let entities = products.reduce(
        (entities: { [id: number]: ProductListItem }, product: ProductListItem) => {
          return {
            ...entities,
            [product.id]: product,
          };
        },
        {...state.entities}
      );
      if (action.payload.next) {
        offset += 12;
      }
      entities = {...state.entities, ...entities};
      return {
        ...state,
        loading: false,
        loaded: true,
        count,
        next,
        previous,
        entities,
        offset,
        errors: null
      };
    }

    case productsActions.LOAD_SEARCHABLE_PRODUCTS_SUCCESS: {
      const products = action.payload.data.results;
      let {count, next, previous} = action.payload.data;
      let offset = 0;
      let entities = products.reduce(
        (entities: { [id: number]: ProductListItem }, product: ProductListItem) => {
          return {
            ...entities,
            [product.id]: product,
          };
        },
        {}
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        count,
        next,
        previous,
        entities,
        offset,
        errors: null
      };
    }


    case productsActions.LOAD_PRODUCTS_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        errors: action.payload
      };
  }
  return state;
}

export const getProductsEntities = (state: ProductsListState) => state.entities;
export const getProductsLoading = (state: ProductsListState) => state.loading;
export const getProductsLoaded = (state: ProductsListState) => state.loaded;
export const getProductsErrors = (state: ProductsListState) => state.errors;
export const getProductsMeta = (state: ProductsListState) => {
  if (state) {
    let {count, next, previous, offset} = state;
    return {count, next, previous, offset};
  }
};
