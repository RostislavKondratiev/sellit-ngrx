import { createSelector } from '@ngrx/store';

import * as reducers from './../reducers';
import * as detailsReducer from './../reducers/product-details.reducer';

export const selectDetailsState = createSelector(
  reducers.getProductsState,
  (state: reducers.ProductsState) => {
    return state.details;
  }
);

export const getDetails = createSelector(
  selectDetailsState,
  detailsReducer.getProductsDetails
);

export const getDetailsLoading = createSelector(
  selectDetailsState,
  detailsReducer.getProductsDetailsLoading
);

export const getDetailsLoaded = createSelector(
  selectDetailsState,
  detailsReducer.getProductsDetailsLoaded
);
