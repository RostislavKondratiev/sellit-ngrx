import {createSelector} from '@ngrx/store';

import * as reducers from './../reducers';
import * as productsReducer from './../reducers/products.reducer';

export const selectProductsState = createSelector(
  reducers.getProductsState,
  (state: reducers.ProductsState) => {
    console.log(state);
    return state.list;
  }
);

export const getProductsEntities = createSelector(
  selectProductsState,
  productsReducer.getProductsEntities
);


export const getAllProducts = createSelector(
  getProductsEntities,
  (entities) => Object.keys(entities).map((id) => entities[id])
);

export const getProductsLoading = createSelector(
  selectProductsState,
  productsReducer.getProductsLoading
);

export const getProductsErrors = createSelector(
  selectProductsState,
  productsReducer.getProductsErrors
);

export const getProductsLoaded = createSelector(
  selectProductsState,
  productsReducer.getProductsLoaded
);

export const getProductsPagination = createSelector(
  selectProductsState,
  productsReducer.getProductsMeta
);
