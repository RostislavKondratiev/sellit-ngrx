import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as rootStore from '../store';

@Injectable()
export class InitializeService {
  constructor(private store: Store<any>) {
  }

  public checkUser() {
    this.store.dispatch(new rootStore.GetSession());
  }
}
