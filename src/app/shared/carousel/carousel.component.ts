import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'si-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarouselComponent implements OnInit, OnChanges {
  public images: string[];
  @Output() public remove = new EventEmitter();
  public carouselConfig: NgxCarousel;

  constructor(private cd: ChangeDetectorRef) {
  }

  @Input()
  public set slides(val) {
    this.images = val.map((item) => item.photo ? item.photo : item);
  }

  public ngOnInit() {
    this.carouselConfig = {
      grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
      slide: 1,
      speed: 400,
      interval: 5000,
      animation: 'lazy',
      point: {
        visible: true,
      },
      load: 2,
      loop: true,
      touch: true
    };
  }

  public changeSlide() {
    this.cd.markForCheck();
  }

  public ngOnChanges(changes) {
    console.log(changes);
  }

  public removeItem(index) {
    this.remove.emit(index);
  }
}
