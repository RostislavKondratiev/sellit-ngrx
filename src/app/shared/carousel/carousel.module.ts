import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxCarouselModule} from 'ngx-carousel';
import {CarouselComponent} from './carousel.component';
import 'hammerjs';

const COMPONENTS = [
  CarouselComponent
];

@NgModule({
  imports: [
    CommonModule,
    NgxCarouselModule,
  ],
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class CarouselModule {

}
