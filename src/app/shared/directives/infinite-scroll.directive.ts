import { Directive, EventEmitter, HostListener, Inject, OnDestroy, OnInit, Output } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[siInfiniteScroll]'
})
export class InfiniteScrollDirective implements OnInit, OnDestroy {
  @Output() public scroll = new EventEmitter<boolean>();
  private debouncer = new Subject();
  private until = new Subject();

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  public ngOnInit() {
    this.debouncer.pipe(
      takeUntil(this.until),
      debounceTime(100),
    )
      .subscribe((res) => {
        this.scroll.emit(true);
      });
  }

  public ngOnDestroy() {
    this.until.next(true);
  }

  @HostListener('window:scroll', ['$event'])
  public onScroll(event) {
    const scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop || 0;
    if (scrollTop + window.innerHeight + 100 >= this.document.body.scrollHeight) {
      this.debouncer.next(true);
    }
  }
}
