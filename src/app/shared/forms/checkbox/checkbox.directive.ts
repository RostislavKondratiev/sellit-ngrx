import { Directive, ElementRef, Input, Optional, Self } from '@angular/core';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { CheckboxComponent } from './checkbox.component';
import { BaseInput } from '../base-input';
import { FormFieldControl } from '../form-field-control';

@Directive({
  selector: 'app-checkbox[appInput]',
  providers: [{provide: FormFieldControl, useExisting: CheckboxDirective}]
})
export class CheckboxDirective extends BaseInput {

  protected _disabled = false;

  constructor(protected elem: ElementRef,
              private checkbox: CheckboxComponent,
              @Optional() @Self() protected ngControl: NgControl,
              @Optional() protected _parentForm: NgForm,
              @Optional() protected _parentFormGroup: FormGroupDirective) {
    super(elem, ngControl, _parentForm, _parentFormGroup);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(value: any) {
    this._disabled = !!value;
    this.checkbox.setDisabledState(this._disabled);
  }
}
