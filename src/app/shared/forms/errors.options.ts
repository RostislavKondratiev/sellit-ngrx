import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';

export function defaultErrorStateMatcher(control: FormControl, form: FormGroupDirective | NgForm) {
  const isSubmitted = form && form.submitted;
  return !!(control.invalid && (control.touched || isSubmitted));
}
