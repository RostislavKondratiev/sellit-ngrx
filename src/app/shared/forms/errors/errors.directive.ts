import { AfterViewInit, Directive, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroupDirective } from '@angular/forms';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ErrorDetails } from './errors.interface';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/pluck';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[siErrors]',
  exportAs: 'siErrors'
})
export class ErrorsDirective implements OnChanges, AfterViewInit, OnDestroy {

  @Input('siErrors')
  public passedControl: FormControl;

  public subject = new BehaviorSubject<ErrorDetails>(null);

  public control: AbstractControl;

  @Output() public hasError = new EventEmitter<boolean>();

  public ready = false;

  private subscription: Subscription;
  private until = new Subject();

  constructor(private form: FormGroupDirective) {
  }

  public get errors() {
    if (!this.ready) {
      return;
    }
    return this.control.errors;
  }

  // public get hasErrors() {
  //   return !!this.errors;
  // }

  // public hasError(name: string, conditions: ErrorOptions): boolean {
  //   return this.checkPropState('invalid', name, conditions);
  // }

  // public isValid(name: string, conditions: ErrorOptions): boolean {
  //   return this.checkPropState('valid', name, conditions);
  // }

  public getError(name: string) {
    if (!this.ready) {
      return;
    }
    return this.control.getError(name);
  }

  public ngOnChanges() {
    this.control = this.passedControl;
  }

  public ngAfterViewInit() {
    this.checkStatus();
    this.subscription = this.control.statusChanges
      .pipe(
        takeUntil(this.until)
      )
      .subscribe(this.checkStatus.bind(this));
  }


  public ngOnDestroy() {
    this.until.next(true);
  }

  // private checkPropState(prop: string, name: string, conditions: ErrorOptions): boolean {
  //   if (!this.ready) {
  //     return;
  //   }
  //   const controlPropsState = (
  //     !conditions || toArray(conditions).every((condition: string) => this.control[condition])
  //   );
  //   if (name.charAt(0) === '*') {
  //     return this.control[prop] && controlPropsState;
  //   }
  //   return (
  //     prop === 'valid' ? !this.control.hasError(name) : this.control.hasError(name) && controlPropsState
  //   );
  // }

  private checkStatus() {
    const control = this.control;
    const errors = control.errors;
    this.ready = true;
    if (!errors) {
      return;
    }
    console.log(errors);
    for (const errorName in errors) {
      if (errors.hasOwnProperty(errorName)) {
        this.subject.next({control, errorName, errorValue: errors[errorName]});
      }
    }
  }

}
