import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SiInputDirective } from './si-input.directive';
import { SiLabelDirective } from './si-label.directive';
import { InputContainerComponent } from './input-container/input-container.component';
import { ErrorDirective } from './errors/error.directive';
import { ErrorsDirective } from './errors/errors.directive';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CheckboxDirective } from './checkbox/checkbox.directive';

const DIRECTIVES = [
  SiInputDirective,
  CheckboxDirective,
  SiLabelDirective,
  ErrorsDirective,
  ErrorDirective
];

const COMPONENTS = [
  InputContainerComponent,
  CheckboxComponent
];
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ...COMPONENTS,
    ...DIRECTIVES
  ],
  exports: [
    ...COMPONENTS,
    ...DIRECTIVES
  ]
})
export class SiFormsModule {
}
