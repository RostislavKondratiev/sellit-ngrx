import { Directive, HostBinding, Input } from '@angular/core';
import { FormFieldControl } from './form-field-control';
import { BaseInput } from './base-input';

@Directive({
  selector: 'input[siInput], textarea[siInput]',
  providers: [{provide: FormFieldControl, useExisting: SiInputDirective}],
})
export class SiInputDirective extends BaseInput {
  @HostBinding('disabled')
  protected _disabled = false;

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(value: any) {
    this._disabled = !!value;
  }

  @HostBinding('placeholder')
  protected _placeholder = '';

  @Input()
  get placeholder() {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
  }
}
