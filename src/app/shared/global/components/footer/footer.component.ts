import { Component } from '@angular/core';

@Component({
  selector: 'si-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  public date = new Date().getFullYear();
}
