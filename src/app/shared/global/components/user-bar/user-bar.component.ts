import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { UserModel } from '../../../../core/models/auth.model';

@Component({
  selector: 'si-user-bar',
  templateUrl: './user-bar.component.html',
  styleUrls: ['./user-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserBarComponent {
  @Input() public user: UserModel;
  @Output() public logout = new EventEmitter();
}
