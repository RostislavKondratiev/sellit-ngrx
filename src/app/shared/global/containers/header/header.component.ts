import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as rootStore from './../../../../store';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { UserModel } from '../../../../core/models/auth.model';

@Component({
  selector: 'si-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: UserModel;

  private until = new Subject();

  constructor(private store: Store<rootStore.RootState>) {
  }

  public ngOnInit() {
    this.store.select(rootStore.getUser)
      .pipe(
        takeUntil(this.until)
      )
      .subscribe((user) => {
        console.log(user);
        this.user = user;
      });

  }

  public logout() {
    this.store.dispatch(new rootStore.CleanSession());
  }

  public ngOnDestroy() {
    this.until.next(true);
  }
}
