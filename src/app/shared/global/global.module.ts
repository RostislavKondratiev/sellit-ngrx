import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './containers/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { UserBarComponent } from './components/user-bar/user-bar.component';
import { SiFormsModule } from '../forms/si-forms.module';
import { RouterModule } from '@angular/router';

const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  UserBarComponent,
];

@NgModule({
  imports: [
    CommonModule,
    SiFormsModule,
    RouterModule
  ],
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class GlobalModule {

}
