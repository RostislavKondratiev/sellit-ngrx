import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'si-bouncer',
  templateUrl: './bouncer.component.html',
  styleUrls: ['./bouncer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BouncerComponent {
  @Input() public message = 'Now Loading...';
}
