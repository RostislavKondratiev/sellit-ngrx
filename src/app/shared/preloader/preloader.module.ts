import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BouncerComponent } from './bouncer/bouncer.component';

const COMPONENTS = [
  BouncerComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class PreloaderModule {

}
