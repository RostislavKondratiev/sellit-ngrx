import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { LocalStorageService } from '../../core/services/localstorage.service';

import * as sessionActions from '../actions/session.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { UserModel } from '../../core/models/auth.model';

@Injectable()
export class SessionEffects {

  constructor(private actions$: Actions,
              private localStorage: LocalStorageService) {
  }

  @Effect()
  public setSession$ = this.actions$
    .pipe(
      ofType(sessionActions.SET_SESSION),
      map((action: sessionActions.SetSession) => action.payload),
      map((user) => {
        this.localStorage.setStorageItem('user', user);
        return new sessionActions.GetSession();
      }));

  @Effect()
  public getSession$ = this.actions$
    .pipe(
      ofType(sessionActions.GET_SESSION),
      switchMap(() => {
        return this.localStorage
          .getStorageItem('user')
          .pipe(
            map((user: UserModel) => new sessionActions.GetSessionSuccess(user)),
            catchError(() => of(new sessionActions.GetSessionFail()))
          );
      })
    );

  @Effect()
  public cleanSession$ = this.actions$
    .pipe(
      ofType(sessionActions.CLEAN_SESSION),
      map(() => {
        this.localStorage.setStorageItem('user', null);
        return new sessionActions.GetSession();
      })
    );
}
