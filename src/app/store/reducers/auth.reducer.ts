import * as authActions from '../actions';

export interface AuthState {
  loading: boolean;
  loaded: boolean;
}

export const InitialState = {
  loading: false,
  loaded: false
};

export function reducer(state: AuthState = InitialState, action: authActions.AuthAction): AuthState {
  switch (action.type) {
    case authActions.LOGIN:
    case authActions.SIGNUP:
      return {...state, loading: true};

    case authActions.SIGNUP_SUCCESS:
    case authActions.LOGIN_SUCCESS:
      return {
        loading: false,
        loaded: true
      };

    case authActions.SIGNUP_FAIL:
    case authActions.LOGIN_FAIL:
      return {loading: false, loaded: false};
  }

  return state;
}


