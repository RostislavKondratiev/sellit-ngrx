import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import * as sessionReducer from './session.reducer';
import { ActionReducerMap } from '@ngrx/store';
import { routerReducer, RouterReducerState, RouterStateSerializer } from '@ngrx/router-store';


export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}

export interface RootState {
  routerReducer: RouterReducerState<RouterStateUrl>;
  session: sessionReducer.UserState;
}

export const reducers: ActionReducerMap<RootState> = {
  routerReducer: routerReducer,
  session: sessionReducer.reducer
};


export class CustomSerializer implements RouterStateSerializer<RouterStateUrl> {
  public serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const {url} = routerState;
    const {queryParams} = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const {params} = state;

    return {url, queryParams, params};
  }
}
