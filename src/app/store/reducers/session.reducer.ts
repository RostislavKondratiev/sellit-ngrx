import { UserModel } from '../../core/models/auth.model';
import * as sessionActions from '../actions/session.actions';

export interface UserState {
  user: UserModel;
  token: string;
}

export const InitialState: UserState = {
  user: null,
  token: null
};

export function reducer(state = InitialState, action: sessionActions.SessionActions) {
  switch (action.type) {
    case sessionActions.GET_SESSION_SUCCESS: {
      const user = action.payload;
      return {user, token: user.token};
    }

    case sessionActions.CLEAN_SESSION: {
      return InitialState;
    }
  }
  return state;
}

export const getUser = ((state: UserState) => state.user);
export const getToken = ((state: UserState) => {
  console.log(state);
  return state.token;
});
