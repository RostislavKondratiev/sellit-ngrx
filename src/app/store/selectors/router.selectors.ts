import { createFeatureSelector } from '@ngrx/store';
import { RouterStateUrl } from '../reducers/index';
import { RouterReducerState } from '@ngrx/router-store';

export const selectRouterState = createFeatureSelector<RouterReducerState<RouterStateUrl>>(
  'routerReducer'
);

