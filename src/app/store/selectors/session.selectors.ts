import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as sessionReducer from '../reducers/session.reducer';

export const sessionState = createFeatureSelector<sessionReducer.UserState>('session');

export const getUser = createSelector(
  sessionState,
  sessionReducer.getUser
);

export const getToken = createSelector(
  sessionState,
  sessionReducer.getToken
);
